%test AG
hiddenSize = 25;
dataf = '../../data/ppdb-only.txt';
load('../../data/skipwiki25.mat');
fid = fopen(dataf,'r');
data = textscan(fid,'%s%s%d', 'delimiter','\t');
play_data = {};
for i=1:1:length(data{1})
    play_data{end+1} = {data{1}{i} data{2}{i} data{3}(i)};
end

global wordMap;
wordMap = containers.Map(words,1:length(words));

temp={};
for i=1:1:length(play_data)
    temp{end+1}=[WordLookup(play_data{i}{1}) WordLookup(play_data{i}{2}) play_data{i}{3}];
end
play_data=temp;

p = randperm(length(play_data));
play_data=play_data(p);
sample = play_data(1:20);

for i=11:1:20
    sample{i}(3) = -1;
end

[We_new, sample_new, wordsT] = limitWords_justwords(sample, We_orig, words);

params.lambda_t=0; %just to prevent changing code
params.lambda_w=0.0005;
params.margin=1;
params.data = sample_new;
params.batchsize = 5;
params.epochs = 10;
params.etat=0.05;
params.etaw=0.50;
params.test=0;
params.evaluate=0;
params.quiet=1;
params.save=0;
newWe = AGWords(params, hiddenSize, wordsT, We_new);

for i=1:1:20
    [i sample_new{i}(3)]
    [dot(newWe(:,sample_new{i}(1)), newWe(:,sample_new{i}(2))) dot(We_orig(:,sample_new{i}(1)), We_orig(:,sample_new{i}(2)))]
end