function [] = train_single(l1,frac,output,datafile,batchsize)
addpath(genpath('../code/'));
load('../data/skipwiki25.mat');
config1
l1
frac
output
datafile
batchsize
global params;
params.constraints=rnnoptions.constraints;
params.lambda_w=l1;
hiddenSize=rnnoptions.hiddenSize;
params.etat=rnnoptions.etat;
params.etaw=rnnoptions.etaw;
params.margin=rnnoptions.margin;
params.test=0;

temp = strcat(rnnoptions.output,'_');
temp = strcat(temp,num2str(l1));
temp = strcat(temp,'_');
temp = strcat(temp,num2str(batchsize));
temp = strcat(temp,'_');
temp = strcat(temp,output);
output=temp;

fid = fopen(datafile,'r');
data = textscan(fid,'%s%s%d', 'delimiter','\t');
play_data = {};

for i=1:1:length(data{1})
    play_data{end+1} = {data{1}{i} data{2}{i} data{3}(i)};
end

global wordMap;
wordMap = containers.Map(words,1:length(words));
temp={};
for i=1:1:length(play_data)
    l = play_data{i}{3};
    if(l == 0)
        continue;
    end
    temp{end+1}=[WordLookup(play_data{i}{1}) WordLookup(play_data{i}{2}) play_data{i}{3}];
end
play_data=temp;

p = randperm(length(play_data));
train_data=play_data(p);

params.epochs=rnnoptions.epochs;
params.save=rnnoptions.save;
params.evaluate=rnnoptions.evaluate;
params.quiet=rnnoptions.quiet;
params.data= train_data(1:round(frac*length(train_data)));
fprintf('Training on %i data using %f\n',length(params.data),l1);

params.batchsize = batchsize;
params.constraints=rnnoptions.constraints;
fprintf('Training on %d instances.\n',length(params.data));

AGWords(params, hiddenSize, words, We_orig, output);
end