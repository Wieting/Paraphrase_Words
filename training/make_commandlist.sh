python make_tti_commands_single.py ../data/ppdb-words.txt words-ppdb-orig 0.05 "../models/skipwiki25_words_" > word_commands_test.txt
python make_tti_commands_single.py ../data/ppdb-only.txt words-ppdb-only 0.05 "../models/skipwiki25_words_" >> word_commands_test.txt
python make_tti_commands_single.py ../data/ppdb-syn.txt words-ppdb-syn 0.05 "../models/skipwiki25_words_" >> word_commands_test.txt
python make_tti_commands_single.py ../data/ppdb-ant-pro.txt words-ppdb-ant-pro 0.05 "../models/skipwiki25_words_" >> word_commands_neg_test.txt
python make_tti_commands_single.py ../data/ppdb-syn-ant-pro.txt words-ppdb-syn-ant-pro 0.05 "../models/skipwiki25_words_" >> word_commands_neg_test.txt

python make_tti_commands_single.py ../data/ppdb-words.txt words-ppdb-orig 1.0 "../models/skipwiki25_words_" > word_commands.txt
python make_tti_commands_single.py ../data/ppdb-only.txt words-ppdb-only 1.0 "../models/skipwiki25_words_" >> word_commands.txt
python make_tti_commands_single.py ../data/ppdb-syn.txt words-ppdb-syn 1.0 "../models/skipwiki25_words_" >> word_commands.txt
python make_tti_commands_single.py ../data/ppdb-ant-pro.txt words-ppdb-ant-pro 1.0 "../models/skipwiki25_words_" >> word_commands.txt
python make_tti_commands_single.py ../data/ppdb-syn-ant-pro.txt words-ppdb-syn-ant-pro 1.0 "../models/skipwiki25_words_" >> word_commands.txt