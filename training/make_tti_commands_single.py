import sys
import os

data = sys.argv[1]
fname = sys.argv[2]
frac = sys.argv[3]
outfile = sys.argv[4]


lambda2=[0.0001,0.00001,0.000001,0.0000001]
bs=[25,50,75,100]

for j in range(len(bs)):
    for i in range(len(lambda2)):
        cmd = str(lambda2[i])+", "+frac+", '"+fname+"', '"+data+"', "+str(bs[j])
        cmd = "matlab -nodisplay -nodesktop -nojvm -nosplash -r \"train_single("+cmd+");quit\""
        print cmd
