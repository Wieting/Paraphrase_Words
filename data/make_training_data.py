#ppdb only
file = open('words.txt','r')
lines = file.readlines()
fout = open('ppdb-words.txt','w')
for i in lines:
    i=i.strip()
    if(len(i) > 0):
        fout.write(i+"\t 1.0\n")
fout.close()

#ppdb only
file = open('ppdb_data.txt','r')
lines = file.readlines()
fout = open('ppdb-only.txt','w')
for i in lines:
    i=i.strip()
    if(len(i) > 0):
        fout.write(i+"\t 1.0\n")
fout.close()

#ppdb+syn
file = open('ppdb_data.txt','r')
ppdblines = file.readlines()
fout = open('ppdb-syn.txt','w')
file = open('syn_data.txt','r')
synlines = file.readlines()
for i in ppdblines:
    i=i.strip()
    if(len(i) > 0):
        fout.write(i+"\t 1.0\n")
for i in synlines:
    i=i.strip()
    if(len(i) > 0):
        fout.write(i+"\t 1.0\n")
fout.close()

#ppdb+ant+pro
file = open('ppdb_data.txt','r')
ppdblines = file.readlines()
fout = open('ppdb-ant-pro.txt','w')
file = open('ant_data.txt','r')
antlines = file.readlines()
file = open('pronouns_data.txt','r')
prolines = file.readlines()
for i in ppdblines:
    i=i.strip()
    if(len(i) > 0):
        fout.write(i+"\t 1.0\n")
for i in antlines:
    i=i.strip()
    if(len(i) > 0):
        fout.write(i+"\t -1.0\n")
for i in prolines:
    i=i.strip()
    if(len(i) > 0):
        fout.write(i+"\t -1.0\n")
fout.close()

#ppdb+syn+ant+pro
file = open('ppdb_data.txt','r')
ppdblines = file.readlines()
fout = open('ppdb-syn-ant-pro.txt','w')
file = open('ant_data.txt','r')
antlines = file.readlines()
file = open('pronouns_data.txt','r')
prolines = file.readlines()
file = open('syn_data.txt','r')
synlines = file.readlines()
for i in ppdblines:
    i=i.strip()
    if(len(i) > 0):
        fout.write(i+"\t 1.0\n")
for i in synlines:
    i=i.strip()
    if(len(i) > 0):
        fout.write(i+"\t 1.0\n")
for i in antlines:
    i=i.strip()
    if(len(i) > 0):
        fout.write(i+"\t -1.0\n")
for i in prolines:
    i=i.strip()
    if(len(i) > 0):
        fout.write(i+"\t -1.0\n")
fout.close()