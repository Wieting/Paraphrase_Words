function [F,num_unk] = lookup_softmatch(m, w, print_backoff)
% m should be a map mapping words to their indices in the vocabulary
% w should be the cell array of words to find embedding vectors for
% F is the vector of indices in m for all words in w.

unkstring = 'UUUNKKK';
ptbtokens = {'-LRB-' '(';'-RRB-' ')'; '-LCB-' '{'; '-RCB-' '}'; '-LSB-' '['; '-RSB-' ']'; '``' '"'; '''''' '"'; '``' ''''''};
%ptblrb = '-LRB-';
%ptbrrb = '-RRB-';
%ptblcb = '-LCB-';
%ptbrcb = '-RCB-';
%ptblsb = '-LSB-';
%ptbrsb = '-RSB-';
%lrb = '(';
%rrb = ')';
%lcb = '{';
%rcb = '}';
%lsb = '[';
%rsb = ']';

num_unk = 0;
% Go through all words in w.
numw = length(w);
F = zeros(1,numw);
for i=1:numw,
    % See if the current word is in m as is.
    ww = w{i};
    isThere = isKey(m, {ww});
    relevantind = {1};
    if (isThere == 1),
        relevantind = values(m, {ww});
    else
        % If not, search for the lowercased version of the word.
        wLowercase = lower(ww);
        isThere2 = isKey(m, {wLowercase});
        if (isThere2 == 1),
            relevantind = values(m, {wLowercase});
            if print_backoff == 1,
        			%fprintf('Matched lc %s\n', wLowercase);
        		end
        else
	        % If not there, search for the version of the word with 
	        % the first letter capitalized
	        wFirstCap = regexprep(ww,'(\<[a-z])','${upper($1)}');
	        isThere3 = isKey(m, {wFirstCap});
	        if (isThere3 == 1),
	            relevantind = values(m, {wFirstCap});
	            if print_backoff == 1,
	        		%fprintf('Matched fc %s\n', wFirstCap);
	        	end
	        else
		        % If not there, search for the all-caps version of the word. 
	            wAllCaps = upper(ww);
	            isThere4 = isKey(m, {wAllCaps});
	            if (isThere4 == 1),
	                relevantind = values(m, {wAllCaps});
                    if print_backoff == 1,
                        %fprintf('Matched ac %s\n', wAllCaps);
                    end
                else
                    % If not there, try special PTB token conversions.
                    %[relevantind, isThere5] = lookup_ptb_tok(m, ww, ptbtokens, print_backoff);
                    %if isThere5 == 0,
                        % Else, just map to UNK.
                        relevantind = values(m, {unkstring});
                        if print_backoff == 1,
                            %fprintf('Unknown word %s\n', w{i});
                        end
                        num_unk = num_unk + 1;
                    %end
	            end
	        end
	    end
    end
    F(i) = relevantind{1};
end

