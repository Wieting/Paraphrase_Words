function correlation=compute_sim_corr(ven, vocaben, gold_corr_en1, gold_corr_en2, goldsims, type)
% ven should be the embeddings for the input words
% vocaben should be the vocabulary/dictionary for the input words
% gold_corr_en1 is the first column of the "true" rank ordering
% gold_corr_en2 is the second column of the "true" rank ordering
% goldsims holds the gold standard similarity values

switch nargin
    case 5
        type = 'cosine';
end

vocab_indices = (1:length(vocaben))';
vocab_map = containers.Map(vocaben, vocab_indices);

% http://www.mathworks.com/help/matlab/ref/containers.mapclass.html

relevantinds1 = lookup_softmatch(vocab_map, gold_corr_en1);
relevantinds2 = lookup_softmatch(vocab_map, gold_corr_en2);

% Compute all pairwise cosine distances
if(strcmp(type,'cosine') ==1)
    D = pdist2(ven, ven, 'cosine');
end

ddd = length(relevantinds1);
sims = zeros(ddd,1);

for i = 1:ddd,
    %gold_corr_en1(i)
    %gold_corr_en2(i)
    %D(relevantinds1{i}, relevantinds2{i})
    if(strcmp(type,'cosine') ==1)
        sims(i) = -D(relevantinds1(i), relevantinds2(i));
    else
        sims(i) = dot(ven(relevantinds1(i),:), ven(relevantinds2(i),:));
    end
end

%[sorted_sims, sorted_inds] = sort(sims);
%for i = 1:ddd,
%    fprintf('%s %s %f\n', gold_corr_en1{sorted_inds(i)}, gold_corr_en2{sorted_inds(i)}, sims(sorted_inds(i)));
%end
corrmat = [sims goldsims];
%corrmat = [sorted_inds [1:ddd]'];
C = corr(corrmat, 'type', 'Spearman');
correlation = C(1,2);

